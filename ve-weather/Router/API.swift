//
//  API.swift
//  Router
//
//  Created by mohamed on 29/12/2015.
//  Copyright © 2015 Carrefour. All rights reserved.
//

import Foundation

// MARK: Completion Closures
public typealias Success = (NSDictionary?) -> Void
public typealias Failure = (NSDictionary?, _ code:Int?) -> Void
public typealias Completion = (NSDictionary, NSError) -> Void

// MARK: API Class
public class API {
    
    enum Method{
        case currentWeather
        case every3Hours
        case daily

        func url() -> String{
            switch self {
            case .currentWeather:
                return "weather?units=metric&id=\(CITY_ID)&APPID=" + APP_ID
            case .every3Hours:
                return "forecast?units=metric&id=\(CITY_ID)&APPID=" + APP_ID
            case .daily:
                return "forecast/daily?units=metric&id=\(CITY_ID)&mode=json&units=metric&cnt=6&APPID=" + APP_ID
            }
        }
    }

    public class func setBaseUrl(url: String) {
        Router.baseURLString = url
    }

    public class func call(request: Router, returnsData:Bool? = true, success: Success?, failure: Failure?) {
        print("URL = \(request.requestURL)")
        
        let session = URLSession.shared
        session.dataTask(with: request.requestURL){ (data, response, error) -> Void in
            guard let httpResponse = response as? HTTPURLResponse else { failure?([:], 500); return }
            print(httpResponse.statusCode)
            if (httpResponse.statusCode == 200) {
                let jsonResult = self.dataToJSON(data: data!)
                success?(jsonResult)
            }
            else {
                failure?([:], httpResponse.statusCode)
            }
            
        }.resume()
    }
    
    private class func dataToJSON(data:Data) -> NSDictionary {
        do {
            if let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? NSDictionary {
                return jsonResult
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return [:]
    }

}
