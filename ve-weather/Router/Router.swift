//
//  Router.swift
//  MonSherif
//
//  Created by mohamed on 19/10/2015.
//  Copyright © 2016 MeetPhone. All rights reserved.
//

import Foundation

// MARK: Router
public enum Router {
    
    #if DEBUG
        static var baseURLString = "http://api.openweathermap.org/data/2.5/"
    #else
        static var baseURLString = "http://api.openweathermap.org/data/2.5/"
    #endif
    
    // api calls
    case GET(path: String)
    
    // api methods
    var method : String {
        switch self {
        case .GET:
            return "GET"
        }
    }
    
    // api paths
    var path: String {
        switch self {
        case .GET(path: let path):
            return path
        }
    }
    
    // api url request
    public var requestURL: URLRequest {
        
        let url = URL(string: Router.baseURLString + path)!
        var request:URLRequest = URLRequest(url: url)
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        switch self {
        case .GET(path: _):
            return request
        }
    }
}


