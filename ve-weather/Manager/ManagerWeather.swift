//
//  ManageWeather.swift
//  VE-Weather
//
//  Created by Quentin Gallois on 3/07/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import Foundation
import RealmSwift

class ManagerWeather {
    static let shared = ManagerWeather()

    /*
     CREATE DATA
     */
    public func createCurrentWeatherData(weatherData:NSDictionary) {
        self.cleanCurrentWeather()
        let realm = try! Realm()
        
        guard let main          = weatherData.object(forKey: "main") as? NSDictionary else { return }
        guard let temp          = main.object(forKey: "temp") as? Double else { return }
        guard let weatherList   = weatherData.object(forKey: "weather") as? [NSDictionary] else { return }
        if weatherList.count <= 0 { return }
        let weather             = weatherList[0]
        guard let iconId        = weather.object(forKey: "id") as? Int else { return }
        
        let calendar = Calendar.current
        let date = Date()
        let hour = calendar.component(.hour, from: date)

        let currentWeatherObject                = CurrentWeather()
        currentWeatherObject.date               = date
        currentWeatherObject.temp               = temp
        currentWeatherObject.weatherIcon        = fetchIcon(iconId: iconId, hour:hour)
        try! realm.write {
            realm.add(currentWeatherObject)
        }
    }

    
    public func createDailyWeatherData(weatherData:[NSDictionary]) {
        self.cleanDailyWeather()
        let realm = try! Realm()

        let calendar = Calendar.current

        for w in weatherData {
            guard let temp          = w.object(forKey: "temp") as? NSDictionary else { continue }
            guard let tempMin       = temp.object(forKey: "min") as? Double else { continue }
            guard let tempMax       = temp.object(forKey: "max") as? Double else { continue }
            guard let dt       = w.object(forKey: "dt") as? Int else { continue }
            guard let weatherList   = w.object(forKey: "weather") as? [NSDictionary] else { return }
            if weatherList.count <= 0 { continue }
            let weather             = weatherList[0]
            guard let iconId        = weather.object(forKey: "id") as? Int else { return }
            
            let dateSince1970 = Date(timeIntervalSince1970: Double(dt))
            var components = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: dateSince1970)
            components.hour = 2
            components.minute = 0
            components.second = 0

            if let date = calendar.date(from: components) {
                let weatherDailyObject                   = DailyWeather()
                weatherDailyObject.date                  = date
                    weatherDailyObject.tempMin           = tempMin
                weatherDailyObject.tempMax               = tempMax
                weatherDailyObject.weatherIcon           = fetchIcon(iconId: iconId, hour:12)
                try! realm.write {
                    realm.add(weatherDailyObject)
                }
            }
        }
    }


    public func createDetailsWeather(weatherData:[NSDictionary]) {
        self.cleanDetailsWeather()
        let realm = try! Realm()

        for w in weatherData {
            guard let date_txt      = w.object(forKey: "dt_txt") as? String else { continue }
            if let date             = date_txt.dateFromWeatherDate {
                guard let main          = w.object(forKey: "main") as? NSDictionary else { continue }
                guard let temp          = main.object(forKey: "temp") as? Double else { continue }
                guard let weatherList   = w.object(forKey: "weather") as? [NSDictionary] else { return }
                if weatherList.count <= 0 { continue }
                let weather             = weatherList[0]
                guard let iconId        = weather.object(forKey: "id") as? Int else { return }
                
                let calendar = Calendar.current
                let hour = calendar.component(.hour, from: date)
                let weatherObject                   = DetailsWeather()
                weatherObject.date                  = date
                weatherObject.temp                  = temp
                weatherObject.weatherIcon           = fetchIcon(iconId: iconId, hour:hour)
                try! realm.write {
                    realm.add(weatherObject)
                }
            }
            else {
                continue
            }
        }
    }

    
    /*
     GET DATA
     */

    public func getCurrentWeather() -> CurrentWeather {
        let realm = try! Realm()
        let currentWeather = realm.objects(CurrentWeather.self).first
        if currentWeather != nil {
            return currentWeather!
        }
        return CurrentWeather()
    }
    
    public func getDetailsWeather(by date:Date, currentWeather:Bool) -> Array<DetailsWeather> {
        var dateStart = date.setHours(hour:10)
        var dateEnd = dateStart.addDays(nb_days: 1)
        

            print(Date())
        if currentWeather {
            let currentDate = Date()
            dateStart = currentDate.setHours(hour:currentDate.getHours())
            dateEnd = dateStart.addDays(nb_days: 1)
        }
        
        print("dateStart")
        print(dateStart)
        print(dateStart.getHours())
        
        print("dateEnd")
        print(dateEnd)
        print(dateEnd.getHours())
        
        let realm = try! Realm()
        let detailsWeather = realm.objects(DetailsWeather.self).filter("(date BETWEEN %@)", [dateStart, dateEnd])
        return Array(detailsWeather)
    }
    
    public func getDailysWeather() -> Array<DailyWeather> {
        let realm = try! Realm()
        let dailyWeather = realm.objects(DailyWeather.self).sorted(byKeyPath: "date")
        return Array(dailyWeather)
    }
    
    
    /*
     DELETE DATA
     */
    private func cleanCurrentWeather() {
        let realm = try! Realm()
        try! realm.write {
            let allWeather = realm.objects(CurrentWeather.self)
            realm.delete(allWeather)
        }
    }

    private func cleanDailyWeather() {
        let realm = try! Realm()
        try! realm.write {
            let allWeather = realm.objects(DailyWeather.self)
            realm.delete(allWeather)
        }
    }

    private func cleanDetailsWeather() {
        let realm = try! Realm()
        try! realm.write {
            let allWeather = realm.objects(DetailsWeather.self)
            realm.delete(allWeather)
        }
    }
    
    
    private func fetchIcon(iconId:Int, hour:Int) -> String {
        var iconName = ""
        switch iconId {
        case 200, 201, 202, 210, 211, 212, 221, 230, 231, 232:
            iconName = "thunderstorm"
        case 300, 301, 302, 310, 311, 312, 313, 314, 321, 511, 520, 521, 522, 531:
            iconName = "big_rain"
        case 500, 501, 502, 503, 504, 701, 711, 721, 731, 741, 751, 761, 762, 771, 781:
            iconName = "small_rain"
        case 600, 601, 602, 611, 612, 615, 616, 620, 621, 622:
            iconName = "snow"
        case 800:
            iconName = "sunny"
        case 801:
            iconName = "few_clouds"
        case 802, 803, 804:
            iconName = "cloudy"
        default:
            iconName = "thunderstorm"
        }
        
        if hour <= 5 || 19 <= hour {
            iconName += "_night"
        }
        return iconName
    }

    
}
