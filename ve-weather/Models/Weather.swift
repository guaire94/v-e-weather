//
//  User.swift
//  UCGroup
//
//  Created by Quentin Gallois on 06/02/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import Foundation
import RealmSwift

class CurrentWeather: Object {
    dynamic var date                = Date()
    dynamic var temp                = 0.0
    dynamic var weatherIcon         = ""
}

class DailyWeather: Object {
    dynamic var date                = Date()
    dynamic var tempMin             = 0.0
    dynamic var tempMax             = 0.0
    dynamic var weatherIcon         = ""
}

class DetailsWeather: Object {
    dynamic var date                = Date()
    dynamic var temp                = 0.0
    dynamic var weatherIcon         = ""
}
