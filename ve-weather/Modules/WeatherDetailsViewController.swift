//
//  ViewController.swift
//  ve-weather
//
//  Created by Quentin Gallois on 30/06/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import UIKit

class WeatherDetailsViewController: UIViewController {

    @IBOutlet weak var loading: UIActivityIndicatorView!

    @IBOutlet weak var dayView: UIView!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var noDataView: UIView!

    @IBOutlet weak var weatherDate: UILabel!
    @IBOutlet weak var weatherIconDay: UIImageView!
    @IBOutlet weak var weatherTempDay: UILabel!

    @IBOutlet weak var weatherHourDetail1: UILabel!
    @IBOutlet weak var weatherIconDetail1: UIImageView!
    @IBOutlet weak var weatherTempDetail1: UILabel!

    @IBOutlet weak var weatherHourDetail2: UILabel!
    @IBOutlet weak var weatherIconDetail2: UIImageView!
    @IBOutlet weak var weatherTempDetail2: UILabel!

    @IBOutlet weak var weatherHourDetail3: UILabel!
    @IBOutlet weak var weatherIconDetail3: UIImageView!
    @IBOutlet weak var weatherTempDetail3: UILabel!

    @IBOutlet weak var weatherHourDetail4: UILabel!
    @IBOutlet weak var weatherIconDetail4: UIImageView!
    @IBOutlet weak var weatherTempDetail4: UILabel!
    
    var selectedDailyWeather : DailyWeather!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.detailsView.backgroundColor = .clear
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.displayWeather()
        self.resizeAllLabelIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func displayWeather() {
        var detailsWeather = [DetailsWeather]()

        if selectedDailyWeather.date <= Date() {
            detailsWeather      = ManagerWeather.shared.getDetailsWeather(by: selectedDailyWeather.date, currentWeather: true)
        }
        else {
            detailsWeather      = ManagerWeather.shared.getDetailsWeather(by: selectedDailyWeather.date, currentWeather: false)
        }
        let icon                = selectedDailyWeather.weatherIcon + "_pink"
        
        weatherDate.text        = selectedDailyWeather.date.frenchFormat
        weatherIconDay.image    = UIImage(named: icon)
        weatherTempDay.text     = "\(Int(selectedDailyWeather.tempMin))° \(Int(selectedDailyWeather.tempMax))°"
        
        if detailsWeather.count > 3 {
            let detailsWeatherDetails1      = detailsWeather[0]
            let detailsWeatherDetails2      = detailsWeather[1]
            let detailsWeatherDetails3      = detailsWeather[2]
            let detailsWeatherDetails4      = detailsWeather[3]

            self.detailsView.isHidden       = false
            self.noDataView.isHidden        = true
            
            self.weatherHourDetail1.text     = "\(detailsWeatherDetails1.date.getHours())h"
            self.weatherIconDetail1.image    = UIImage(named: detailsWeatherDetails1.weatherIcon)
            self.weatherTempDetail1.text     = "\(Int(detailsWeatherDetails1.temp))°"

            self.weatherHourDetail2.text     = "\(detailsWeatherDetails2.date.getHours())h"
            self.weatherIconDetail2.image    = UIImage(named: detailsWeatherDetails2.weatherIcon)
            self.weatherTempDetail2.text     = "\(Int(detailsWeatherDetails2.temp))°"

            self.weatherHourDetail3.text     = "\(detailsWeatherDetails3.date.getHours())h"
            self.weatherIconDetail3.image    = UIImage(named: detailsWeatherDetails3.weatherIcon)
            self.weatherTempDetail3.text     = "\(Int(detailsWeatherDetails3.temp))°"

            self.weatherHourDetail4.text     = "\(detailsWeatherDetails4.date.getHours())h"
            self.weatherIconDetail4.image    = UIImage(named: detailsWeatherDetails4.weatherIcon)
            self.weatherTempDetail4.text     = "\(Int(detailsWeatherDetails4.temp))°"
        }
        else {
            self.detailsView.isHidden   = true
            self.noDataView.isHidden    = false
        }
    }
    
    func resizeAllLabelIfNeeded() {
        weatherDate.resizeIfNeeded()
        weatherTempDay.resizeIfNeeded()
        
        weatherHourDetail1.resizeIfNeeded()
        weatherTempDetail1.resizeIfNeeded()
        
        weatherHourDetail2.resizeIfNeeded()
        weatherTempDetail2.resizeIfNeeded()
        
        weatherHourDetail3.resizeIfNeeded()
        weatherTempDetail3.resizeIfNeeded()
        
        weatherHourDetail4.resizeIfNeeded()
        weatherTempDetail4.resizeIfNeeded()
    }
    
    
    @IBAction func closeToggle(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}


