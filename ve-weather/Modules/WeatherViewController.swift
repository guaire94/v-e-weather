//
//  ViewController.swift
//  ve-weather
//
//  Created by Quentin Gallois on 30/06/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import UIKit
import ReachabilitySwift

class WeatherViewController: UIViewController {

    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var refreshButton: UIButton!

    @IBOutlet weak var todayView: UIView!
    @IBOutlet weak var weekView: UIView!

    @IBOutlet weak var weatherDateDay1: UILabel!
    @IBOutlet weak var weatherIconDay1: UIImageView!
    @IBOutlet weak var weatherTempDay1: UILabel!

    @IBOutlet weak var weatherDateDay2: UILabel!
    @IBOutlet weak var weatherIconDay2: UIImageView!
    @IBOutlet weak var weatherTempDay2: UILabel!

    @IBOutlet weak var weatherDateDay3: UILabel!
    @IBOutlet weak var weatherIconDay3: UIImageView!
    @IBOutlet weak var weatherTempDay3: UILabel!

    @IBOutlet weak var weatherDateDay4: UILabel!
    @IBOutlet weak var weatherIconDay4: UIImageView!
    @IBOutlet weak var weatherTempDay4: UILabel!

    @IBOutlet weak var weatherDateDay5: UILabel!
    @IBOutlet weak var weatherIconDay5: UIImageView!
    @IBOutlet weak var weatherTempDay5: UILabel!

    @IBOutlet weak var weatherDateDay6: UILabel!
    @IBOutlet weak var weatherIconDay6: UIImageView!
    @IBOutlet weak var weatherTempDay6: UILabel!

    var selectedDailyWeather : DailyWeather!
    var haveConnection = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkReachability()
        self.resizeAllLabelIfNeeded()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func checkReachability() {
        self.loading.startAnimating()
        self.weekView.isHidden = true
        self.todayView.isHidden = true

        let reachability = Reachability()!
        reachability.whenReachable = { reachability in
            DispatchQueue.main.async {
                self.haveConnection = true
                self.refreshButton.setImage(UIImage(named:"refresh"), for: .normal)
                self.fetchCurrentWeather()
            }
        }
        reachability.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                self.haveConnection = false
                self.refreshButton.setImage(UIImage(named:"nowifi"), for: .normal)
                self.fetchWeatherDone()
            }
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func fetchCurrentWeather() {
        API.call(request: Router.GET(path: API.Method.currentWeather.url()), returnsData: true,
                 success: { (data) in
                    DispatchQueue.main.async {
                        if data != nil {
                            ManagerWeather.shared.createCurrentWeatherData(weatherData: data!)
                            self.fetchDailyWeather()
                        }
                        else {
                            self.showAlert(title: "Erreur connexion", message: "Impossible de récuperer la météo d'aujourd'hui")
                            self.loading.stopAnimating()
                        }
                    }
        })
        { (data, statutCode) in
            self.showAlert(title: "Erreur connexion", message: "Impossible de récuperer la météo d'aujourd'hui")
            self.loading.stopAnimating()
        }
    }
    
    func fetchDailyWeather() {
        API.call(request: Router.GET(path: API.Method.daily.url()), returnsData: true,
                 success: { (data) in
                    DispatchQueue.main.async {
                        guard let weatherData = data?.object(forKey: "list") as? [NSDictionary] else { return }
                        ManagerWeather.shared.createDailyWeatherData(weatherData: weatherData)
                        self.fetchForecastWeather()
                    }
        })
        { (data, statutCode) in
            self.showAlert(title: "Erreur connexion", message: "Impossible de récuperer la météo des prochains jours")
            self.loading.stopAnimating()
        }
    }
    
    func fetchForecastWeather() {
        API.call(request: Router.GET(path: API.Method.every3Hours.url()), returnsData: true,
                 success: { (data) in
                    DispatchQueue.main.async {
                        if data != nil {
                            guard let weatherData = data?.object(forKey: "list") as? [NSDictionary] else { return }
                            ManagerWeather.shared.createDetailsWeather(weatherData: weatherData)
                            self.fetchWeatherDone()
                        }
                        else {
                            self.showAlert(title: "Erreur connexion", message: "Impossible de récuperer le details de la météo des prochains jours")
                            self.loading.stopAnimating()
                        }
                    }
        })
        { (data, statutCode) in
            self.showAlert(title: "Erreur connexion", message: "Impossible de récuperer le details de la météo des prochains jours")
            self.loading.stopAnimating()
        }
    }
    
    
    func fetchWeatherDone() {
        self.loading.stopAnimating()
        self.weekView.alpha     = 0.0
        self.todayView.alpha    = 0.0
        self.todayView.isHidden  = false
        self.weekView.isHidden  = false
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       options: UIViewAnimationOptions.curveEaseIn,
                       animations: { () -> Void in
                        self.weekView.alpha     = 1.0
                        self.todayView.alpha    = 1.0
        }, completion: { (finished) -> Void in
        })
        self.displayWeather()
    }
    
    func displayWeather() {
        let currentWeather      = ManagerWeather.shared.getCurrentWeather()
        let dailyWeather        = ManagerWeather.shared.getDailysWeather()
        let icon = currentWeather.weatherIcon + "_pink"
        
        weatherDateDay1.text    = currentWeather.date.frenchFormat
        weatherIconDay1.image   = UIImage(named: icon)
        weatherTempDay1.text    = "\(Int(currentWeather.temp))°"
        
        if (dailyWeather.count > 1) {
            let dailyWeatherDay2    = dailyWeather[1]
            weatherDateDay2.text    = dailyWeatherDay2.date.frenchFormat
            weatherIconDay2.image   = UIImage(named: dailyWeatherDay2.weatherIcon)
            weatherTempDay2.text    = "\(Int(dailyWeatherDay2.tempMin))° \(Int(dailyWeatherDay2.tempMax))°"
        }
        
        if (dailyWeather.count > 2) {
            let dailyWeatherDay3    = dailyWeather[2]
            weatherDateDay3.text    = dailyWeatherDay3.date.frenchFormat
            weatherIconDay3.image   = UIImage(named: dailyWeatherDay3.weatherIcon)
            weatherTempDay3.text    = "\(Int(dailyWeatherDay3.tempMin))° \(Int(dailyWeatherDay3.tempMax))°"
        }
        
        if (dailyWeather.count > 3) {
            let dailyWeatherDay4    = dailyWeather[3]
            weatherDateDay4.text    = dailyWeatherDay4.date.frenchFormat
            weatherIconDay4.image   = UIImage(named: dailyWeatherDay4.weatherIcon)
            weatherTempDay4.text    = "\(Int(dailyWeatherDay4.tempMin))° \(Int(dailyWeatherDay4.tempMax))°"
        }
        
        if (dailyWeather.count > 4) {
            let dailyWeatherDay5    = dailyWeather[4]
            weatherDateDay5.text    = dailyWeatherDay5.date.frenchFormat
            weatherIconDay5.image   = UIImage(named: dailyWeatherDay5.weatherIcon)
            weatherTempDay5.text    = "\(Int(dailyWeatherDay5.tempMin))° \(Int(dailyWeatherDay5.tempMax))°"
        }
        
        if (dailyWeather.count > 5) {
            let dailyWeatherDay6    = dailyWeather[5]
            weatherDateDay6.text    = dailyWeatherDay6.date.frenchFormat
            weatherIconDay6.image   = UIImage(named: dailyWeatherDay6.weatherIcon)
            weatherTempDay6.text    = "\(Int(dailyWeatherDay6.tempMin))° \(Int(dailyWeatherDay6.tempMax))°"
        }

    }
    
    func resizeAllLabelIfNeeded() {
        weatherDateDay1.resizeIfNeeded()
        weatherTempDay1.resizeIfNeeded()
        
        weatherDateDay2.resizeIfNeeded()
        weatherTempDay2.resizeIfNeeded()
        
        weatherDateDay3.resizeIfNeeded()
        weatherTempDay3.resizeIfNeeded()
        
        weatherDateDay4.resizeIfNeeded()
        weatherTempDay4.resizeIfNeeded()
        
        weatherDateDay5.resizeIfNeeded()
        weatherTempDay5.resizeIfNeeded()
        
        weatherDateDay6.resizeIfNeeded()
        weatherTempDay6.resizeIfNeeded()
    }
    
    @IBAction func detailsWeatherToggle(_ sender: UIButton) {
        let dailyWeather        = ManagerWeather.shared.getDailysWeather()
        switch sender.tag {
        case 1:
            if (dailyWeather.count > 0) {
                selectedDailyWeather    = dailyWeather[0]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        case 2:
            if (dailyWeather.count > 1) {
                selectedDailyWeather    = dailyWeather[1]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        case 3:
            if (dailyWeather.count > 2) {
                selectedDailyWeather    = dailyWeather[2]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        case 4:
            if (dailyWeather.count > 3) {
                selectedDailyWeather    = dailyWeather[3]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        case 5:
            if (dailyWeather.count > 4) {
                selectedDailyWeather    = dailyWeather[4]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        case 6:
            if (dailyWeather.count > 5) {
                selectedDailyWeather    = dailyWeather[5]
                self.performSegue(withIdentifier: "WeatherDetailsViewController", sender: self)
            }
            break
        default:
            break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WeatherDetailsViewController" {
            guard let vc = segue.destination as? WeatherDetailsViewController else { return }
            vc.selectedDailyWeather = selectedDailyWeather
        }
    }
    
    @IBAction func refreshToggle(_ sender: UIButton) {
        self.checkReachability()
    }
    
}


