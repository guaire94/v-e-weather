//
//  LabelExtension.swift
//  ve-weather
//
//  Created by Quentin Gallois on 04/07/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    public func resizeIfNeeded() {
        self.adjustsFontSizeToFitWidth = true;
        self.minimumScaleFactor = 0.5
    }
}
