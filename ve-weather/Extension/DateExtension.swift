//
//  Extension.swift
//  GeolocalizationTest
//
//  Created by Quentin Gallois on 22/12/2016.
//  Copyright © 2016 Quentin Gallois. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func dayNumberOfWeek() -> Int {
        return Calendar.current.dateComponents([.weekday], from: self).weekday!
    }
        
    var frenchFormat: String {
        return Formatter.frenchDateFormat.string(from: self)
    }
    
    func addDays(nb_days:Int) -> Date {
        if let newDate = Calendar.current.date(byAdding: .day, value: nb_days, to: self) {
            return newDate
        }
        return Date()
    }
    
    func getHours() -> Int {
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: self)
        return hour
    }

    func setHours(hour:Int) -> Date {
        let calendar = Calendar.current
        var dateComponents = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self)
        dateComponents.hour = hour
        dateComponents.minute = 0
        dateComponents.second = 0
        if let date = calendar.date(from: dateComponents) {
            return date
        }
        return Date()
    }
}

extension Formatter {
    static let weatherDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    static let frenchDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "fr_FR_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "dd MMMM yyyy"
        return formatter
    }()

}

extension String {
    var dateFromWeatherDate: Date? {
        return Formatter.weatherDateFormat.date(from: self)
    }
}

