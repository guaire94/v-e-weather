//
//  ViewControllerExtension.swift
//  ve-weather
//
//  Created by Quentin Gallois on 05/07/2017.
//  Copyright © 2017 Quentin Gallois. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    public func showAlert(title:String, message:String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
